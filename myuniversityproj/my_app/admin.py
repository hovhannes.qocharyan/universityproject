from django.contrib import admin
from my_app.models import University, Subject

class UniversityAdmin(admin.ModelAdmin):
    list_display = ('universityName', 'address')


admin.site.register(University, UniversityAdmin)
admin.site.register(Subject)
#admin.site.register(Student)



