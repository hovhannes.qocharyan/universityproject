from django.shortcuts import render
from django.http import HttpResponse
from my_app.models import University, Subject
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET"])
def university(request):
    university = University()
    universityes = university.get_university_list()
    return render(request, 'university/university.html', context={'temp': universityes})


@require_http_methods(["GET"])
def subject(request):
    subject = Subject()
    subjects = subject.get_subject_list()
    return render(request, 'subject/subject.html', context={'temp':subjects})


@require_http_methods(["GET"])
def university_detail(request, id):
    university = University()
    details = university.get_university_details(id=id)
    return render(request, 'university/university_detail.html', context={'temp': details[0], 'subjects':details[1]})


@require_http_methods(["GET"])
def edit_university(request,id):
    return render(request,'university/edit.html')


