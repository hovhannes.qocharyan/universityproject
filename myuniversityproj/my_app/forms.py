from django import forms

class University_Form(forms.Form):
    universityName = forms.CharField(max_length=50)
    address = forms.CharField(max_length=50)
