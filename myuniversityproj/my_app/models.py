from django.db import models
from django.shortcuts import reverse

# Create your models here.
class University(models.Model):
    universityName = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    subjects = models.ManyToManyField('Subject')

    def __str__(self):
        return self.universityName

    def get_absolute_url(self):
        return reverse('univerity_detail_url', kwargs={'id': self.id})

    def get_university_list(self):
        universityes = University.objects.all()
        return universityes

    def get_university_details(self,id):
        university = University.objects.get(id__iexact=id)
        subjects = university.subjects.all()
        details = (university, subjects)
        return details


    class Meta:
        db_table='university'


# class Student(models.Model):
#     firstName = models.CharField(max_length=15)
#     lastName = models.CharField(max_length=15)
#     phoneNumber = models.CharField(max_length=10)
#     birthdate = models.DateField()
#     courses = models.ManyToManyField('SubjectList')
#
#     def __str__(self):
#         return '{}'.format(self.firstName+" "+self.lastName)
#
#     class Meta:
#         db_table="student"


class Subject(models.Model):
    subjectName = models.CharField(max_length=25)

    def __str__(self):
        return self.subjectName

    def get_subject_list(self):
        subjects = Subject.objects.all()
        return subjects

    class Meta:
        db_table = "subject"







