from django.urls import path
from my_app.views import university ,subject ,university_detail ,edit_university

urlpatterns = [
    path('university', university),
    path('subject', subject),
    path('university/<id>/', university_detail, name='univerity_detail_url'),
    path('university/edit/<id>/', edit_university)
]